﻿using System.Collections.Generic;
using BLL.Contracts.DTOs;

namespace BLL.Contracts
{
    public interface IOnlineLibrary
    {
        int CountPagesForSearch(string keyword, int categoryId, int itemsPerPage);
        IEnumerable<IBook> SearchBooks(string keyword, int categoryId, int page, int count);
        int CountPagesBooks(int itemsPerPage);
        int CountPagesCategories(int itemsPerPage);
        int CountPagesFavouriteBooks(int itemsPerPage);
        IBook CreateBook(IBook book);
        ICategory CreateCategory(ICategory category);
        IFavouriteBook CreateFavouriteBook(IFavouriteBook favouriteBook);
        bool DeleteBook(int id);
        bool DeleteCategory(int id);
        bool DeleteFavouriteBook(int id);
        IBook GetBookById(int id);
        IEnumerable<IBook> GetBooks(int page, int count);
        IEnumerable<IBook> GetBooksFromCategory(int categoryId, int page, int count);
        IEnumerable<ICategory> GetCategories(int page, int count);
        ICategory GetCategoryById(int id);
        IFavouriteBook GetFavouriteBookByUserIdAndBookId(int bookId, int userId);
        IEnumerable<IFavouriteBook> GetFavouriteBooksForUserId(int id, int page, int count);
        bool UpdateBook(IBook book);
        bool UpdateCategory(ICategory book);
        bool UpdateFavouriteBook(IFavouriteBook book);

        int CountCommentsByBook(int bookId);
        IComment CreateComment(IComment comment);
        IEnumerable<IComment> GetCommentsByBook(int bookId, int page, int count);
        bool UpdateComment(IComment comment);
        bool DeleteComment(int id);
        IComment GetComment(int id);

    }
}