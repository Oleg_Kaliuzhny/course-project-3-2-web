﻿using System;

namespace BLL.Contracts.DTOs
{
    public interface IComment
    {
        int Id { get; set; }
        int UserId { get; set; }
        int BookId { get; set; }
        string Text { get; set; }
        DateTime CreationDate { get; set; }
    }
}
