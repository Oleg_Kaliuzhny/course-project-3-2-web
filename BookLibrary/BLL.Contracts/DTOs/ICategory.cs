﻿namespace BLL.Contracts.DTOs
{
    public interface ICategory
    {
        string Description { get; set; }
        int Id { get; set; }
        string Title { get; set; }
    }
}