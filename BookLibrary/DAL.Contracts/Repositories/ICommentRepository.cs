﻿using System.Collections.Generic;
using DAL.Contracts.DTOs;

namespace DAL.Contracts.Repositories
{
    public interface ICommentRepository
    {
        int CountCommentsByBook(int bookId);
        int CreateComment(IComment model);
        void DeleteComment(int id);
        IEnumerable<IComment> ReadComments(int bookId, int skip, int take);
        IComment ReadComment(int id);
        void UpdateComment(IComment model);
    }
}