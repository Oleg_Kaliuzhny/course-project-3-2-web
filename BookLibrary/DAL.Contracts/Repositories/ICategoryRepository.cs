﻿using System.Collections.Generic;
using DAL.Contracts.DTOs;

namespace DAL.Contracts.Repositories
{
    public interface ICategoryRepository
    {
        int CreateCategory(ICategory model);
        void DeleteCategory(int id);
        IEnumerable<ICategory> ReadCategories(int skip, int take);
        ICategory ReadCategoryById(int id);
        void UpdateCategory(ICategory model);
        int CountPages(int itemsPerPage);
    }
}