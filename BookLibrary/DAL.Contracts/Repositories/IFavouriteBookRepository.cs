﻿using System.Collections.Generic;
using DAL.Contracts.DTOs;

namespace DAL.Contracts.Repositories
{
    public interface IFavouriteBookRepository
    {
        int CreateFavouriteBook(IFavouriteBook model);
        void DeleteFavouriteBook(int id);
        IFavouriteBook ReadFavouriteBookByBookIdAndUserId(int bookId, int userId);
        IEnumerable<IFavouriteBook> ReadFavouriteBooksForUserId(int id, int skip, int take);
        void UpdateFavouriteBook(IFavouriteBook model);
        int CountPages(int itemsPerPage);
    }
}