﻿using System.Collections.Generic;
using DAL.Contracts.DTOs;

namespace DAL.Contracts.Repositories
{
    public interface IBookRepository
    {
        int CountPagesForSearch(string keyword, int categoryId, int itemsPerPage);
        IEnumerable<IBook> SearchBooks(string keyword, int categoryId, int skip, int take);
        int CreateBook(IBook model);
        void DeleteBook(int id);
        IBook ReadBookById(int id);
        IEnumerable<IBook> ReadBooks(int skip, int take);
        IEnumerable<IBook> ReadBooksFromCategory(int categoryId, int skip, int take);
        void UpdateBook(IBook model);
        int CountPages(int itemsPerPage);
    }
}