﻿using System;

namespace DAL.Contracts.DTOs
{
    public interface IBook
    {
        int CategoryId { get; set; }
        string Description { get; set; }
        int Id { get; set; }
        string Link { get; set; }
        string Title { get; set; }
        string Logo { get; set; }
        string Author { get; set; }
        DateTime CreationDate { get; set; }
    }
}