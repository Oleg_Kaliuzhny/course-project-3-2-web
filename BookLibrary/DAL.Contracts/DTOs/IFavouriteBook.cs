﻿namespace DAL.Contracts.DTOs
{
    public interface IFavouriteBook
    {
        int BookId { get; set; }
        int Id { get; set; }
        int UserId { get; set; }
    }
}