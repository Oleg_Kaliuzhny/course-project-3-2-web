﻿using BLL.Contracts;
using BLL.Domains;
using DAL.Contracts.Repositories;
using DAL.Domains.Repositories;

namespace IoC
{
    public static class Factory
    {
        public static IOnlineLibrary GetOnlineLibrary()
        {
            return new OnlineLibrary(GetBookRepository(),
                GetCategoryRepository(),
                GetFavouriteBookRepository(),
                GetCommentRepository());
        }

        static IBookRepository GetBookRepository()
        {
            return new BookRepository();
        }

        static ICategoryRepository GetCategoryRepository()
        {
            return new CategoryRepository();
        }

        static IFavouriteBookRepository GetFavouriteBookRepository()
        {
            return new FavouriteBookRepository();
        }

        static ICommentRepository GetCommentRepository()
        {
            return new CommentRepository();
        }
    }
}
