﻿using Identity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;

namespace Identity.Infrastructure
{
    public class ApplicationRoleManager : RoleManager<CustomRole, int>, IDisposable
    {
        public ApplicationRoleManager(IRoleStore<CustomRole, int> store)
            : base(store)
        { }

        public static ApplicationRoleManager Create(
            IdentityFactoryOptions<ApplicationRoleManager> options,
            IOwinContext context)
        {
            return new ApplicationRoleManager(new
                CustomRoleStore(context.Get<ApplicationDbContext>()));
        }
    }
}