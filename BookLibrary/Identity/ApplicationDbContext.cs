﻿using Identity.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Identity
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, CustomRole,
    int, CustomUserLogin, CustomUserRole, CustomUserClaim>
    { 
        public ApplicationDbContext() 
            : base("name=OnlineLibrary")
        {

        }

        static ApplicationDbContext()
        {
            Database.SetInitializer<ApplicationDbContext>(new IdentityDbInit());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
