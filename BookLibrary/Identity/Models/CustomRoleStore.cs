﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Identity.Models
{
    public class CustomRoleStore : RoleStore<CustomRole, int, CustomUserRole>
    {
        public CustomRoleStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}
