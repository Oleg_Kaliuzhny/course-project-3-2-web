﻿using Identity.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Identity.Models
{
    public class CustomUserStore : UserStore<ApplicationUser, CustomRole, int,
    CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public CustomUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}
