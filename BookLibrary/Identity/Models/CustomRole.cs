﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Identity.Models
{
    public class CustomRole : IdentityRole<int, CustomUserRole>
    {
        public CustomRole() : base() 
        {

        }

        public CustomRole(string name) 
        {
            Name = name;
        }
    }
}
