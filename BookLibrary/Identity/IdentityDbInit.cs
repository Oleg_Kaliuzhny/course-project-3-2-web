﻿using Identity.Infrastructure;
using Identity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Identity
{
    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }
        public void PerformInitialSetup(ApplicationDbContext context)
        {
            ApplicationUserManager userMgr = new ApplicationUserManager(new CustomUserStore(context));
            ApplicationRoleManager roleMgr = new ApplicationRoleManager(new CustomRoleStore(context));

            string roleName = "Administrators";
            string userName = "admin";
            string password = "ilovecakes";
            string email = "my@email.com";
            string firstName = "Admin";
            string lastName = "Administrator";

            if (!roleMgr.RoleExists(roleName))
            {
                roleMgr.Create(new CustomRole(roleName));
            }

            ApplicationUser user = userMgr.FindByName(userName);
            if (user == null)
            {
                userMgr.Create(new ApplicationUser { UserName = userName, Email = email,
                    FirstName = firstName, LastName = lastName }, password);
                user = userMgr.FindByName(userName);
            }

            if (!userMgr.IsInRole(user.Id, roleName))
            {
                userMgr.AddToRole(user.Id, roleName);
            }
        }
    }
}
