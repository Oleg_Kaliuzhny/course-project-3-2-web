// <auto-generated />
namespace Identity.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class InitialSettings : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(InitialSettings));
        
        string IMigrationMetadata.Id
        {
            get { return "201801031403064_InitialSettings"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
