﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookLibrary.Models;
using Identity.Infrastructure;
using Microsoft.AspNet.Identity.Owin;

namespace BookLibrary.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class AdminController : Controller
    {
        public AdminController()
        {

        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
    }
}