﻿using BLL.Contracts;
using BookLibrary.Models;
using Identity.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Web;
using System.Web.Mvc;

namespace BookLibrary.Controllers
{
    public class CommentController : Controller
    {
        public CommentController()
        {
            _onlineLibrary = IoC.Factory.GetOnlineLibrary();
        }

        private IOnlineLibrary _onlineLibrary;

        // POST: Comment
        [Authorize]
        [HttpPost]
        public ActionResult Create([Bind(Include = "BookId,Text")]Comment comment)
        {
            if (!ModelState.IsValid)
            {
                return Content("<div></div>");
            }

            var userId = Request.IsAuthenticated ? Convert.ToInt32(User.Identity.GetUserId()) : -1;
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            comment.UserName = userManager.FindById(userId).UserName;
            comment.CreationDate = DateTime.Now;
            comment.UserId = userId;

            var created = _onlineLibrary.CreateComment(comment);

            var model = new Comment
            {
                BookId = created.BookId,
                UserId = created.UserId,
                Id = created.Id,
                CreationDate = created.CreationDate,
                Text = created.Text,
                UserName = comment.UserName,
            };

            return PartialView("~/Views/Shared/_Comment.cshtml", model);
        }

        // POST: Comment
        [Authorize]
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,BookId,Text,CreationDate,UserId")]Comment comment)
        {
            if (!ModelState.IsValid)
            {
                return Content("<div></div>");
            }

            var userId = Request.IsAuthenticated ? Convert.ToInt32(User.Identity.GetUserId()) : -1;
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            comment.UserName = userManager.FindById(userId).UserName;
            comment.UserId = userId;

            var isUpdated = _onlineLibrary.UpdateComment(comment);

            var model = new Comment
            {
                BookId = comment.BookId,
                UserId = comment.UserId,
                Id = comment.Id,
                CreationDate = comment.CreationDate,
                Text = comment.Text,
                UserName = comment.UserName,
            };

            return PartialView("~/Views/Shared/_Comment.cshtml", model);
        }

        // POST: Comment
        [HttpPost]
        [Authorize]
        public ActionResult Delete(int id)
        {
            var userId = Request.IsAuthenticated ? Convert.ToInt32(User.Identity.GetUserId()) : -1;

            var comment = _onlineLibrary.GetComment(id);

            if (comment != null && comment.UserId == userId)
            {
                _onlineLibrary.DeleteComment(id);
            }

            return Content("<div></div>");
        }
    }
}