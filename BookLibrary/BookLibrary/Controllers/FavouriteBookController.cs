﻿using BLL.Contracts;
using BookLibrary.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookLibrary.Controllers
{
    [Authorize]
    public class FavouriteBookController : Controller
    {
        public FavouriteBookController()
        {
            _onlineLibrary = IoC.Factory.GetOnlineLibrary();
        }

        private IOnlineLibrary _onlineLibrary;

        // GET: Add FavouriteBook
        public ActionResult Add(int? id, string redirectTo)
        {
            if (id.HasValue && Request.IsAuthenticated)
            {
                var userId = Convert.ToInt32(User.Identity.GetUserId());
                var book = _onlineLibrary.GetBookById(id.Value);

                if (book != null)
                {
                    _onlineLibrary.CreateFavouriteBook(new FavouriteBook { BookId = book.Id, UserId = userId });
                }
            }

            return Redirect(redirectTo);
        }

        // GET: Delete FavouriteBook
        public ActionResult Delete(int? id, string redirectTo)
        {
            if (id.HasValue)
                _onlineLibrary.DeleteFavouriteBook(id.Value);

            return Redirect(redirectTo);
        }
    }
}