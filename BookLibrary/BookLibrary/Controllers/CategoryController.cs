﻿using BLL.Contracts;
using BookLibrary.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BookLibrary.Controllers
{
    public class CategoryController : Controller
    {
        public CategoryController()
        {
            _onlineLibrary = IoC.Factory.GetOnlineLibrary();
        }

        private IOnlineLibrary _onlineLibrary;

        // GET: Category
        public ActionResult Index()
        {
            var categories = _onlineLibrary.GetCategories(0, int.MaxValue);
            var models = new List<Category>();
            foreach (var category in categories)
            {
                var model = new Category()
                {
                    Id = category.Id,
                    Title = category.Title,
                    Description = category.Description
                };
                models.Add(model);
            }

            return View(models);
        }

        // GET: Create
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Create()
        {
            return View(new Category());
        }

        // POST: Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Create(Category model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = _onlineLibrary.CreateCategory(model);
            
            if (result == null)
            {
                return View(model);
            }

            return RedirectToAction("Index");
        }

        // GET: Edit
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Edit(int id)
        {
            var category = _onlineLibrary.GetCategoryById(id);

            var model = new Category()
            {
                Id = category.Id,
                Title = category.Title,
                Description = category.Description
            };

            return View(model);
        }

        // POST: Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Edit(Category model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = _onlineLibrary.UpdateCategory(model);

            if (!result)
            {
                return View(model);
            }

            return RedirectToAction("Index");
        }

        // GET: Delete
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Delete(int id)
        {
            var result = _onlineLibrary.DeleteCategory(id);

            return RedirectToAction("Index");
        }
    }
}