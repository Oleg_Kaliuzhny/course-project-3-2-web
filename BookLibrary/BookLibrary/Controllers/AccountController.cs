﻿using BookLibrary.Models.Account;
using Identity.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System;
using Identity.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using BLL.Contracts;
using BookLibrary.Models;

namespace BookLibrary.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IOnlineLibrary _onlineLibrary;

        public AccountController()
        {
            _onlineLibrary = IoC.Factory.GetOnlineLibrary();
        }

        // GET: /Account/SignUp
        [AllowAnonymous]
        public ActionResult SignUp()
        {
            SignUpVM model = new SignUpVM();
            return View(model);
        }

        // POST: /Account/SignUp
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignUp(SignUpVM model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser
                {
                    UserName = model.Login,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                };

                IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Book");
                }
                else
                {
                    AddErrorsFromResult(result);
                }
            }
            return View(model);
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            LoginVM loginVM = new LoginVM();
            return View(loginVM);
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginVM loginVM)
        {
            ApplicationUser user = await UserManager.FindAsync(loginVM.Login, loginVM.Password);

            if (user == null)
            {
                ModelState.AddModelError("", "Invalid login or password.");
            }
            else
            {
                //create cookie
                ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
                    DefaultAuthenticationTypes.ApplicationCookie);

                AuthenticationManager.SignOut();
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = false
                }, ident);
                return RedirectToAction("Index", "Book");
            }

            return View(loginVM);
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Book");
        }

        // GET: /Account/MyProfile
        public ActionResult MyProfile()
        {
            var userId = Convert.ToInt32(User.Identity.GetUserId());
            var currentUser = UserManager.FindById(userId);
            var favouriteBooksRepo = _onlineLibrary.GetFavouriteBooksForUserId(userId, 0, int.MaxValue);
            List<FavouriteBook> favouriteBooks = new List<FavouriteBook>();

            foreach (var fav in favouriteBooksRepo)
            {
                var favModel = new FavouriteBook
                {
                    Id = fav.Id,
                    BookId = fav.BookId,
                    UserId = fav.UserId,
                };
                var book = _onlineLibrary.GetBookById(fav.BookId);
                favModel.Book = new Book { Id = book.Id, Title = book.Title, Link = book.Link,
                    Description = book.Description, CategoryId = book.CategoryId, Author = book.Author,
                Logo = book.Logo, CreationDate = book.CreationDate, };
                favModel.Book.Category = _onlineLibrary.GetCategoryById(book.CategoryId);

                favouriteBooks.Add(favModel);
            }

            var model = new ProfileVM
            {
                Email = UserManager.GetEmail(userId),
                Login = User.Identity.GetUserName(),
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName,
                FavouriteBooks = favouriteBooks,
            };
            return View(model);
        }

        [Authorize]
        public ActionResult EditProfile()
        {
            var userId = Convert.ToInt32(User.Identity.GetUserId());
            var currentUser = UserManager.FindById(userId);
            var model = new ProfileVM
            {
                Email = UserManager.GetEmail(userId),
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName,
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditProfile(ProfileVM model, string currentPassword,
            string newPassword, string repeatNewPassword)
        {
            var userId = Convert.ToInt32(User.Identity.GetUserId());
            var currentUser = UserManager.FindById(userId);

            if (ModelState.IsValid && newPassword == repeatNewPassword)
            {
                currentUser.FirstName = model.FirstName;
                currentUser.LastName = model.LastName;
                currentUser.Email = model.Email;

                IdentityResult result = await UserManager.UpdateAsync(currentUser);

                if (!result.Succeeded)
                {
                    AddErrorsFromResult(result);
                    return View(model);
                }

                if (!string.IsNullOrWhiteSpace(currentPassword))
                {
                    IdentityResult resultPass = await UserManager
                        .ChangePasswordAsync(userId, currentPassword, newPassword);

                    if (!result.Succeeded)
                    {
                        AddErrorsFromResult(resultPass);
                        return View(model);
                    }
                }

                return RedirectToAction("MyProfile");
            }

            return View("EditProfile", model);
        }

        #region Addition
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [NonAction]
        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        #endregion
    }
}