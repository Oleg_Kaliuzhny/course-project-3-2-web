﻿using BLL.Contracts;
using BookLibrary.Models;
using Identity.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookLibrary.Controllers
{
    public class BookController : Controller
    {
        public BookController()
        {
            _onlineLibrary = IoC.Factory.GetOnlineLibrary();
        }

        private IOnlineLibrary _onlineLibrary;

        // GET: Book
        public ActionResult Index(int? page, int? size)
        {
            var userId = Request.IsAuthenticated ? Convert.ToInt32(User.Identity.GetUserId()) : -1;
            int selectedPage = page == null ? 1 : page.Value > 0 ? page.Value : 1;
            int selectedSize = size == null ? 25 : size.Value > 0 ? size.Value : 25;
            PageInfo pageInfo = new PageInfo
            {
                PageSize = selectedSize,
                PageNumber = selectedPage
            };
            pageInfo.PageCount = _onlineLibrary.CountPagesBooks(pageInfo.PageSize);

            ViewBag.CurrentCategory = "";
            ViewBag.PageInfo = pageInfo;

            var books = _onlineLibrary.GetBooks(selectedPage - 1, pageInfo.PageSize);
            var categories = _onlineLibrary.GetCategories(0, int.MaxValue);
            ViewBag.Categories = categories.Select(x => new Category
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description
            });

            var models = new List<Book>();
            foreach (var item in books)
            {
                var model = new Book()
                {
                    Id = item.Id,
                    Title = item.Title,
                    Description = item.Description,
                    Link = item.Link,
                    CategoryId = item.CategoryId,
                    Category = _onlineLibrary.GetCategoryById(item.CategoryId),
                    FavouriteBook = _onlineLibrary.GetFavouriteBookByUserIdAndBookId(item.Id, userId),
                    Logo = item.Logo ?? "https://files.selar.co/product-images/2017/products/demo/preorder-physical-product-selar.co-59d1e525c3a96.jpg",
                    Author = item.Author,
                    CommentCount = _onlineLibrary.CountCommentsByBook(item.Id),
                    CreationDate = item.CreationDate
                };
                models.Add(model);
            }

            return View(models);
        }

        // POST: Search
        [HttpPost]
        public ActionResult Search(Search search, int? page, int? size)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(search.Query))
                {
                    search.Query = "";
                }

                var userId = Request.IsAuthenticated ? Convert.ToInt32(User.Identity.GetUserId()) : -1;

                int selectedPage = page == null ? 1 : page.Value > 0 ? page.Value : 1;
                int selectedSize = size == null ? 25 : size.Value > 0 ? size.Value : 25;
                PageInfo pageInfo = new PageInfo
                {
                    PageSize = selectedSize,
                    PageNumber = selectedPage
                };
                pageInfo.PageCount = _onlineLibrary.CountPagesForSearch(search.Query, 0, pageInfo.PageSize);

                ViewBag.CurrentCategory = "";
                ViewBag.PageInfo = pageInfo;

                var books = _onlineLibrary.SearchBooks(search.Query, 0, selectedPage - 1, pageInfo.PageSize);
                var categories = _onlineLibrary.GetCategories(0, int.MaxValue);
                ViewBag.Categories = categories.Select(x => new Category
                {
                    Id = x.Id,
                    Title = x.Title,
                    Description = x.Description
                });

                var models = new List<Book>();
                foreach (var item in books)
                {
                    var model = new Book()
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Description = item.Description,
                        Link = item.Link,
                        CategoryId = item.CategoryId,
                        Category = _onlineLibrary.GetCategoryById(item.CategoryId),
                        FavouriteBook = _onlineLibrary.GetFavouriteBookByUserIdAndBookId(item.Id, userId),
                        Logo = item.Logo ?? "https://files.selar.co/product-images/2017/products/demo/preorder-physical-product-selar.co-59d1e525c3a96.jpg",
                        Author = item.Author,
                        CommentCount = _onlineLibrary.CountCommentsByBook(item.Id),
                        CreationDate = item.CreationDate
                    };
                    models.Add(model);
                }

                return PartialView("BookList", models);
            }
            return PartialView("BookList", null);
        }

        // GET: Category
        public ActionResult Category(int id)
        {
            var userId = Request.IsAuthenticated ? Convert.ToInt32(User.Identity.GetUserId()) : -1;
            var books = _onlineLibrary.GetBooksFromCategory(id, 0, int.MaxValue);

            var categories = _onlineLibrary.GetCategories(0, int.MaxValue);
            ViewBag.Categories = categories.Select(x => new Category
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description
            });

            ViewBag.CurrentCategory = "in " + _onlineLibrary.GetCategoryById(id).Title;

            var models = new List<Book>();
            foreach (var book in books)
            {
                var model = new Book()
                {
                    Id = book.Id,
                    Title = book.Title,
                    Description = book.Description,
                    Link = book.Link,
                    CategoryId = book.CategoryId,
                    Category = _onlineLibrary.GetCategoryById(book.CategoryId),
                    FavouriteBook = _onlineLibrary.GetFavouriteBookByUserIdAndBookId(book.Id, userId),
                    Logo = book.Logo,
                    CreationDate = book.CreationDate,
                    CommentCount = 0,
                    Author = book.Author,
                };
                models.Add(model);
            }

            return View("Index", models);
        }

        // GET: Create
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Create()
        {
            ViewBag.Categories = _onlineLibrary.GetCategories(0, int.MaxValue);
            return View();
        }

        // POST: Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Create(Book model, HttpPostedFileBase upload, HttpPostedFileBase logo)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (upload != null)
            {
                string fileName = System.IO.Path.GetFileName(upload.FileName);

                fileName = $"{Guid.NewGuid().ToString()}_{fileName}";

                var path = $"~/App_Data/{fileName}";

                var link = $"/Book/Download?file={fileName}";

                upload.SaveAs(Server.MapPath(path));
                model.Link = link;
            }

            if (logo != null)
            {
                string fileName = System.IO.Path.GetFileName(logo.FileName);

                fileName = $"{Guid.NewGuid().ToString()}_{fileName}";

                var path = $"~/App_Data/{fileName}";

                var link = $"/Book/Download?file={fileName}";

                logo.SaveAs(Server.MapPath(path));
                model.Logo = link;
            }

            model.CreationDate = DateTime.Now;
            var result = _onlineLibrary.CreateBook(model);

            if (result == null)
            {
                return View(model);
            }

            return RedirectToAction("Index");
        }

        // GET: Download
        public ActionResult Download(string file)
        {
            string path = Server.MapPath($"~/App_Data/{file}");

            if (!System.IO.File.Exists(path))
            {
                return Redirect("~/Error/NotFound");
            }

            string fileType = "application/pdf";

            string fileName = "file";

            return File(path, fileType, fileName);
        }


        // GET: Edit
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Edit(int id)
        {
            var book = _onlineLibrary.GetBookById(id);

            ViewBag.Categories = _onlineLibrary.GetCategories(0, int.MaxValue);

            var model = new Book()
            {
                Id = book.Id,
                Title = book.Title,
                Description = book.Description,
                CategoryId = book.CategoryId,
                Link = book.Link,
                Category = _onlineLibrary.GetCategoryById(book.CategoryId),
                Logo = book.Logo,
                CreationDate = book.CreationDate,
                CommentCount = 0,
                Author = book.Author,
            };

            return View(model);
        }

        // POST: Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Edit(Book model)
        {
            ViewBag.Categories = _onlineLibrary.GetCategories(0, int.MaxValue);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = _onlineLibrary.UpdateBook(model);

            if (!result)
            {
                return View(model);
            }

            return RedirectToAction("Index");
        }

        // GET: Delete
        [Authorize(Roles = "Administrators,Moderators")]
        public ActionResult Delete(int id)
        {
            var result = _onlineLibrary.DeleteBook(id);

            return RedirectToAction("Index");
        }

        public ActionResult Book(int? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var book = _onlineLibrary.GetBookById(id.Value);

            if (book == null)
                return RedirectToAction("Index");

            var comments = _onlineLibrary.GetCommentsByBook(id.Value, 0, int.MaxValue);

            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            ViewBag.Comments = comments.Select(x => new Comment {
                BookId = x.BookId,
                CreationDate = x.CreationDate,
                Text = x.Text,
                Id = x.Id,
                UserId = x.UserId,
                UserName = userManager?.FindById(x.UserId)?.UserName ?? "DELETED",
            });

            var userId = Request.IsAuthenticated ? Convert.ToInt32(User.Identity.GetUserId()) : -1;

            var model = new Book()
            {
                Id = book.Id,
                Title = book.Title,
                Description = book.Description,
                Link = book.Link,
                CategoryId = book.CategoryId,
                Category = _onlineLibrary.GetCategoryById(book.CategoryId),
                FavouriteBook = _onlineLibrary.GetFavouriteBookByUserIdAndBookId(book.Id, userId),
                Logo = book.Logo ?? "https://files.selar.co/product-images/2017/products/demo/preorder-physical-product-selar.co-59d1e525c3a96.jpg",
                Author = book.Author,
                CommentCount = comments.Count(),
                CreationDate = book.CreationDate
            };

            return View(model);
        }
    }
}