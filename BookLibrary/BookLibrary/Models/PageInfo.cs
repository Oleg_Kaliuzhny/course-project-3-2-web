﻿using System;

namespace BookLibrary.Models
{
    public class PageInfo : PagedList.IPagedList
    {
        public int PageCount { get; set; }

        public int TotalItemCount { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public bool HasPreviousPage { get { return PageCount > PageNumber && PageNumber > 1; } }

        public bool HasNextPage { get { return PageCount > PageNumber; } }

        public bool IsFirstPage { get { return PageNumber == 1; } }

        public bool IsLastPage { get { return PageNumber == PageCount; } }

        public int FirstItemOnPage { get; set; }

        public int LastItemOnPage { get; set; }
    }
}