﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Identity.Infrastructure;
using Microsoft.AspNet.Identity.Owin;

namespace BookLibrary.Models.Helpers
{
    public static class Helpers
    {
        public static MvcHtmlString GetUserName(this HtmlHelper html, string id)
        {
            ApplicationUserManager mgr = HttpContext.Current
                .GetOwinContext().GetUserManager<ApplicationUserManager>();

            return new MvcHtmlString(mgr.FindByIdAsync(Convert.ToInt32(id)).Result.UserName);
        }
    }
}