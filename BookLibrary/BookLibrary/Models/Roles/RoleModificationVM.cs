﻿using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.Roles
{
    public class RoleModificationVM
    {
        [Required]
        public string RoleName { get; set; }
        public string[] IdsToAdd { get; set; }
        public string[] IdsToDelete { get; set; }
    }
}