﻿using Identity.Models;
using System.Collections.Generic;

namespace BookLibrary.Models.Roles
{
    public class RoleEditVM
    {
        public CustomRole Role { get; set; }
        public IEnumerable<ApplicationUser> Members { get; set; }
        public IEnumerable<ApplicationUser> NonMembers { get; set; }
    }
}