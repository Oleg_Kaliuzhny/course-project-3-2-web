﻿using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models
{
    public class Search
    {
        [Display(Name = "Search Book")]
        [MinLength(1)]
        public string Query { get; set; }
        public int CategoryId { get; set; }
    }
}