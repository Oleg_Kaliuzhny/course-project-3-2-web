﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookLibrary.Models.Account
{
    public class ProfileVM
    {
        public string Login { get; set; }
        
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public IEnumerable<FavouriteBook> FavouriteBooks { get; set; }
    }
}