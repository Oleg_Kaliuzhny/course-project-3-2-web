﻿using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models.Account
{
    public class LoginVM
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}