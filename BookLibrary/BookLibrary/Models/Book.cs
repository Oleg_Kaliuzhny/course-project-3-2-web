﻿using BLL.Contracts.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models
{
    public class Book : IBook
    {
        public ICategory Category { get; set; }

        public IEnumerable<ICategory> Categories { get; set; }

        [Required]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]
        public string Link { get; set; }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        public IFavouriteBook FavouriteBook { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public string Logo { get; set; }

        [ScaffoldColumn(false)]
        public string Author { get; set; }

        [ScaffoldColumn(false)]
        public DateTime CreationDate { get; set; }

        [ScaffoldColumn(false)]
        public int CommentCount { get; set; }
    }
}