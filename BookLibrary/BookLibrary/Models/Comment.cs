﻿using BLL.Contracts.DTOs;
using System;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models
{
    public class Comment : IComment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
        [MinLength(2)]
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }
        public string UserName { get; set; }
    }
}