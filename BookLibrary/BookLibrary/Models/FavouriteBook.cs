﻿using BLL.Contracts.DTOs;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models
{
    public class FavouriteBook : IFavouriteBook
    {
        [Required]
        public int BookId { get; set; }

        public Book Book { get; set; }

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}