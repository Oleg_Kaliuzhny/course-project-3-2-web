﻿using BLL.Contracts.DTOs;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Models
{
    public class Category : ICategory
    {
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }
    }
}