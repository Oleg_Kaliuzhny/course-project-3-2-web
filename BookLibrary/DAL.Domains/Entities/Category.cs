﻿namespace DAL.Domains.Entities
{
    public class Category : EntityBase
    {
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
    }
}