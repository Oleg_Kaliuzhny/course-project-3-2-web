﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DAL.Domains.Entities
{
    public class Comment : EntityBase
    {
        public virtual int UserId { get; set; }
        public virtual int BookId { get; set; }
        [MaxLength(1024)]
        public virtual string Text { get; set; }

        public virtual DateTime CreationDate { get; set; }

        [ForeignKey("BookId")]
        public virtual Book Book{ get; set; }
    }
}
