﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Domains.Entities
{
    public class FavouriteBook : EntityBase
    {
        public virtual int BookId { get; set; }
        public virtual int UserId { get; set; }

        [ForeignKey("BookId")]
        public virtual Book Book { get; set; }
    }
}
