﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Domains.Entities
{
    public class Book : EntityBase
    {
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual string Link { get; set; }
        public virtual int CategoryId { get; set; }
        public virtual string Logo { get; set; }
        public virtual string Author { get; set; }
        public virtual DateTime CreationDate { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
    }
}
