namespace DAL.Domains.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BookUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Author", c => c.String());
            AddColumn("dbo.Books", "CreationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Books", "CommentCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "CommentCount");
            DropColumn("dbo.Books", "CreationDate");
            DropColumn("dbo.Books", "Author");
        }
    }
}
