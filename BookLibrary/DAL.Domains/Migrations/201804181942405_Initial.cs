namespace DAL.Domains.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Logo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "Logo");
        }
    }
}
