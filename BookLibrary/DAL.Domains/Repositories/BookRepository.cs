﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTOs;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class BookRepository : BaseRepository<Entities.Book>, IBookRepository
    {
        // 0 = all categories
        public IEnumerable<IBook> SearchBooks(string keyword, int categoryId, int skip, int take)
        {
            using (var context = new Context())
            {
                var items = context.Set<Book>().Where(x => (x.Title.ToLower().Contains(keyword) ||
                x.Author.ToLower().Contains(keyword)) && 
                categoryId == 0 ? true : x.CategoryId == categoryId)
                .ToList();

                return items.Select(x => EntityToDTO(x));
            }
        }

        public int CountPagesForSearch(string keyword, int categoryId, int itemsPerPage)
        {
            using (var context = new Context())
            {
                var count = context.Set<Book>().Where(x => (x.Title.ToLower().Contains(keyword) ||
                x.Description.ToLower().Contains(keyword)) &&
                categoryId == 0 ? true : x.CategoryId == categoryId)
                    .Count();
                var pages = count / itemsPerPage;

                if (count % itemsPerPage > 0)
                {
                    pages++;
                }

                return pages;
            }
        }

        public int CreateBook(IBook model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteBook(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<IBook> ReadBooks(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IEnumerable<IBook> ReadBooksFromCategory(int categoryId, int skip, int take)
        {
            var selected = base.Read(x => x.CategoryId == categoryId, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IBook ReadBookById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateBook(IBook model)
        {
            base.Update(DTOToEntity(model));
        }

        private IBook EntityToDTO(Entities.Book item)
        {
            return new DTOs.Book
            {
                Id = item.Id,
                Description = item.Description,
                Title = item.Title,
                CategoryId = item.CategoryId,
                Link = item.Link,
                Logo = item.Logo,
                CreationDate = item.CreationDate,
                Author = item.Author,
            };
        }

        private Entities.Book DTOToEntity(IBook item)
        {
            return new Entities.Book
            {
                Id = item.Id,
                Description = item.Description,
                Title = item.Title,
                CategoryId = item.CategoryId,
                Link = item.Link,
                Logo = item.Logo,
                Author = item.Author,
                CreationDate = item.CreationDate
            };
        }

        protected override void MapForUpdate(Book selected, Book entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.Title = entity.Title;
            selected.Description = entity.Description;
            selected.CategoryId = entity.CategoryId;
            selected.Link = entity.Link;
            selected.Logo = entity.Logo;
            selected.Author = entity.Author;
            selected.CreationDate = entity.CreationDate;
        }
    }
}
