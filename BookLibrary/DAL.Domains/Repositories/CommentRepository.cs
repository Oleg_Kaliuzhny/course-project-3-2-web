﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Contracts.DTOs;
using DAL.Domains.Entities;
using DAL.Contracts.Repositories;

namespace DAL.Domains.Repositories
{
    public class CommentRepository : BaseRepository<Entities.Comment>, ICommentRepository
    {
        public int CountCommentsByBook(int bookId)
        {
            using (var context = new Context())
            {
                var count = context.Set<Comment>().Where(x => (x.BookId == bookId))
                    .Count();
                return count;
            }
        }

        public int CreateComment(IComment model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteComment(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<IComment> ReadComments(int bookId, int skip, int take)
        {
            var selected = base.Read(x => x.BookId == bookId, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IComment ReadComment(int id)
        {
            var selected = base.Read(x => x.Id == id, 0, 1);
            return selected.Select(x => EntityToDTO(x)).FirstOrDefault();
        }

        public void UpdateComment(IComment model)
        {
            base.Update(DTOToEntity(model));
        }

        private IComment EntityToDTO(Entities.Comment item)
        {
            return new DTOs.Comment
            {
                Id = item.Id,
                Text = item.Text,
                BookId = item.BookId,
                UserId = item.UserId,
                CreationDate = item.CreationDate
            };
        }

        private Entities.Comment DTOToEntity(IComment item)
        {
            return new Entities.Comment
            {
                Id = item.Id,
                Text = item.Text,
                BookId = item.BookId,
                UserId = item.UserId,
                CreationDate = item.CreationDate
            };
        }

        protected override void MapForUpdate(Comment selected, Comment entity)
        {
            if (selected == null || entity == null)
                return;

            selected.BookId = entity.BookId;
            selected.Id = entity.Id;
            selected.Text = entity.Text;
            selected.UserId = entity.UserId;
            selected.CreationDate = entity.CreationDate;
        }
    }
}
