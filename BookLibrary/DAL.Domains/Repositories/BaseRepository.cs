﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Domains.Entities;
using System.Data.Entity;
using System.Linq.Expressions;

namespace DAL.Domains.Repositories
{
    public abstract class BaseRepository<TEntity> where TEntity : EntityBase
    {
        public int CountPages(int itemsPerPage)
        {
            using (var context = new Context())
            {
                var count = context.Set<TEntity>()
                    .Count();
                var pages = count / itemsPerPage;

                if (count % itemsPerPage > 0)
                {
                    pages++;
                }

                return pages;
            }
        }

        #region IRepository
        internal int Create(TEntity entity)
        {
            using (Context context = new Context())
            {
                context.Set<TEntity>().Add(entity);
                context.SaveChanges();
            }
            return entity.Id;
        }

        internal void Delete(int id)
        {
            using (var context = new Context())
            {
                var entity = context.Set<TEntity>()
                    .FirstOrDefault(o => o.Id.Equals(id));
                if (entity != null)
                {
                    context.Set<TEntity>().Remove(entity);
                    context.SaveChanges();
                }
            }
        }

        internal IEnumerable<TEntity> Read(Expression<Func<TEntity, bool>> predicate, int skip, int take)
        {
            using (var context = new Context())
            {
                var selected = context.Set<TEntity>()
                    .OrderBy(x => x.Id)
                    .Where(predicate)
                    .Skip(skip)
                    .Take(take)
                    .ToList();

                return selected;
            }
        }

        internal void Update(TEntity entity)
        {
            using (var context = new Context())
            {
                var selected = context.Set<TEntity>()
                    .FirstOrDefault(o => o.Id == entity.Id);
                MapForUpdate(selected, entity);
                context.SaveChanges();
            }
        }

        protected abstract void MapForUpdate(TEntity selected, TEntity entity);
        
        #endregion
    }
}
