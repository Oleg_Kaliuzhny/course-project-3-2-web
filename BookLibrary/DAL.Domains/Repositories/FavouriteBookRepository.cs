﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTOs;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class FavouriteBookRepository : BaseRepository<Entities.FavouriteBook>, IFavouriteBookRepository
    {
        public int CreateFavouriteBook(IFavouriteBook model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteFavouriteBook(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<IFavouriteBook> ReadFavouriteBooksForUserId(int id, int skip, int take)
        {
            var selected = base.Read(x => x.UserId == id, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public IFavouriteBook ReadFavouriteBookByBookIdAndUserId(int bookId, int userId)
        {
            var entity = base.Read(x => x.BookId == bookId && x.UserId == userId, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateFavouriteBook(IFavouriteBook model)
        {
            base.Update(DTOToEntity(model));
        }

        private IFavouriteBook EntityToDTO(Entities.FavouriteBook item)
        {
            return new DTOs.FavouriteBook
            {
                Id = item.Id,
                BookId = item.BookId,
                UserId = item.UserId
            };
        }

        private Entities.FavouriteBook DTOToEntity(IFavouriteBook item)
        {
            return new Entities.FavouriteBook
            {
                Id = item.Id,
                BookId = item.BookId,
                UserId = item.UserId
            };
        }

        protected override void MapForUpdate(FavouriteBook selected, FavouriteBook entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.BookId = entity.BookId;
            selected.UserId = entity.UserId;
        }
    }
}
