﻿using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.DTOs;
using DAL.Contracts.Repositories;
using DAL.Domains.Entities;

namespace DAL.Domains.Repositories
{
    public class CategoryRepository : BaseRepository<Entities.Category>, ICategoryRepository
    {
        public int CreateCategory(ICategory model)
        {
            return base.Create(DTOToEntity(model));
        }

        public void DeleteCategory(int id)
        {
            base.Delete(id);
        }

        public IEnumerable<ICategory> ReadCategories(int skip, int take)
        {
            var selected = base.Read(x => true, skip, take);
            return selected.Select(x => EntityToDTO(x));
        }

        public ICategory ReadCategoryById(int id)
        {
            var entity = base.Read(x => x.Id == id, 0, 1).FirstOrDefault();
            return entity == null ? null : EntityToDTO(entity);
        }

        public void UpdateCategory(ICategory model)
        {
            base.Update(DTOToEntity(model));
        }

        private ICategory EntityToDTO(Entities.Category item)
        {
            return new DTOs.Category
            {
                Id = item.Id,
                Description = item.Description,
                Title = item.Title,
            };
        }

        private Entities.Category DTOToEntity(ICategory item)
        {
            return new Entities.Category
            {
                Id = item.Id,
                Description = item.Description,
                Title = item.Title,
            };
        }

        protected override void MapForUpdate(Category selected, Category entity)
        {
            if (selected == null || entity == null)
                return;
            selected.Id = entity.Id;
            selected.Title = entity.Title;
            selected.Description = entity.Description;
        }
    }
}
