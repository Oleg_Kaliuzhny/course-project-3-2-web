﻿using System.Data.Entity;
using DAL.Domains.Entities;

namespace DAL.Domains
{
    public class Context : DbContext
    {
        public Context()
          : base("Library")
        {

        }

        public DbSet<Book> Books { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<FavouriteBook> FavouriteBooks { get; set; }

        public DbSet<Comment> Comments { get; set; }
    }
}