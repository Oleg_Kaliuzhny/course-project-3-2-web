﻿using DAL.Contracts.DTOs;

namespace DAL.Domains.DTOs
{
    public class FavouriteBook : IFavouriteBook
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int UserId { get; set; }
    }
}
