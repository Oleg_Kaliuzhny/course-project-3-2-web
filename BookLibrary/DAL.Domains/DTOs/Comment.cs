﻿using DAL.Contracts.DTOs;
using System;

namespace DAL.Domains.DTOs
{
    public class Comment : IComment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
