﻿using DAL.Contracts.DTOs;

namespace DAL.Domains.DTOs
{
    public class Category : ICategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
