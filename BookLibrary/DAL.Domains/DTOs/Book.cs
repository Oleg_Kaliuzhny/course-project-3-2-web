﻿using DAL.Contracts.DTOs;
using System;

namespace DAL.Domains.DTOs
{
    public class Book : IBook
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int CategoryId { get; set; }
        public string Logo { get; set; }
        public string Author { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
