﻿using BLL.Contracts.DTOs;
using D = DAL.Contracts.DTOs;

namespace BLL.Domains.DTOs
{
    public class Category : ICategory, D.ICategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
