﻿using BLL.Contracts.DTOs;
using System;
using D = DAL.Contracts.DTOs;

namespace BLL.Domains.DTOs
{
    public class Comment : IComment, D.IComment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
