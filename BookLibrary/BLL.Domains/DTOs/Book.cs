﻿using BLL.Contracts.DTOs;
using System;
using D = DAL.Contracts.DTOs;

namespace BLL.Domains.DTOs
{
    public class Book : IBook, D.IBook
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int CategoryId { get; set; }
        public string Logo { get; set; }
        public string Author { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
