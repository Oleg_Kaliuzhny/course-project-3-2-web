﻿using BLL.Contracts.DTOs;
using D = DAL.Contracts.DTOs;

namespace BLL.Domains.DTOs
{
    public class FavouriteBook : IFavouriteBook, D.IFavouriteBook
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int UserId { get; set; }
    }
}
