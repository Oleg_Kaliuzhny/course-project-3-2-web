﻿using DAL.Contracts.Repositories;
using BLL.Contracts.DTOs;
using System.Linq;
using System.Collections.Generic;
using BLL.Contracts;

namespace BLL.Domains
{
    public class OnlineLibrary : IOnlineLibrary
    {
        public OnlineLibrary(IBookRepository bookRepository, 
            ICategoryRepository categoryRepository, 
            IFavouriteBookRepository favouriteBookRepository,
            ICommentRepository commentRepository)
        {
            _bookRepository = bookRepository;
            _categoryRepository = categoryRepository;
            _favouriteBookRepository = favouriteBookRepository;
            _commentRepository = commentRepository;
        }

        private readonly IBookRepository _bookRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IFavouriteBookRepository _favouriteBookRepository;
        private readonly ICommentRepository _commentRepository;

        #region Books
        public int CountPagesForSearch(string keyword, int categoryId, int itemsPerPage)
        {
            try
            {
                var count = _bookRepository.CountPagesForSearch(keyword, categoryId, itemsPerPage);
                return count;
            }
            catch (System.Data.DataException)
            {
                return 0;
            }
        }

        public IEnumerable<IBook> SearchBooks(string keyword, int categoryId, int page, int count)
        {
            try
            {
                var items = _bookRepository.SearchBooks(keyword, categoryId, page * count, count);
                return items.Select(x => TypeConverter.FromDAL(x));
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public int CountPagesBooks(int itemsPerPage)
        {
            try
            {
                var count = _bookRepository.CountPages(itemsPerPage);
                return count;
            }
            catch (System.Data.DataException)
            {
                return 0;
            }
        }

        public IBook CreateBook(IBook book)
        {
            try
            {
                var id = _bookRepository.CreateBook(TypeConverter.ToDAL(book));
                book.Id = id;
                return book;
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IEnumerable<IBook> GetBooks(int page, int count)
        {
            try
            {
                var items =_bookRepository.ReadBooks(page * count, count);
                return items.Select(x => TypeConverter.FromDAL(x));
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IEnumerable<IBook> GetBooksFromCategory(int categoryId, int page, int count)
        {
            try
            {
                var items = _bookRepository.ReadBooksFromCategory(categoryId, page * count, count);
                return items.Select(x => TypeConverter.FromDAL(x));
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IBook GetBookById(int id)
        {
            try
            {
                var item = _bookRepository.ReadBookById(id);
                return TypeConverter.FromDAL(item);
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public bool UpdateBook(IBook book)
        {
            try
            {
                _bookRepository.UpdateBook(TypeConverter.ToDAL(book));
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }

        public bool DeleteBook(int id)
        {
            try
            {
                _bookRepository.DeleteBook(id);
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }

        #endregion

        #region Categories
        public int CountPagesCategories(int itemsPerPage)
        {
            try
            {
                var count = _categoryRepository.CountPages(itemsPerPage);
                return count;
            }
            catch (System.Data.DataException)
            {
                return 0;
            }
        }

        public ICategory CreateCategory(ICategory category)
        {
            try
            {
                var id = _categoryRepository.CreateCategory(TypeConverter.ToDAL(category));
                category.Id = id;
                return category;
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IEnumerable<ICategory> GetCategories(int page, int count)
        {
            try
            {
                var items = _categoryRepository.ReadCategories(page * count, count);
                return items.Select(x => TypeConverter.FromDAL(x));
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public ICategory GetCategoryById(int id)
        {
            try
            {
                var item = _categoryRepository.ReadCategoryById(id);
                return TypeConverter.FromDAL(item);
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public bool UpdateCategory(ICategory book)
        {
            try
            {
                _categoryRepository.UpdateCategory(TypeConverter.ToDAL(book));
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }

        public bool DeleteCategory(int id)
        {
            try
            {
                _categoryRepository.DeleteCategory(id);
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }
        #endregion

        #region FavouriteBooks
        public int CountPagesFavouriteBooks(int itemsPerPage)
        {
            try
            {
                var count = _favouriteBookRepository.CountPages(itemsPerPage);
                return count;
            }
            catch (System.Data.DataException)
            {
                return 0;
            }
        }

        public IFavouriteBook CreateFavouriteBook(IFavouriteBook favouriteBook)
        {
            try
            {
                var id = _favouriteBookRepository.CreateFavouriteBook(TypeConverter.ToDAL(favouriteBook));
                favouriteBook.Id = id;
                return favouriteBook;
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IEnumerable<IFavouriteBook> GetFavouriteBooksForUserId(int id, int page, int count)
        {
            try
            {
                var items = _favouriteBookRepository.ReadFavouriteBooksForUserId(id, page * count, count);
                return items.Select(x => TypeConverter.FromDAL(x));
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IFavouriteBook GetFavouriteBookByUserIdAndBookId(int bookId, int userId)
        {
            try
            {
                var item = _favouriteBookRepository.ReadFavouriteBookByBookIdAndUserId(bookId, userId);
                return item != null ? TypeConverter.FromDAL(item) : null;
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public bool UpdateFavouriteBook(IFavouriteBook book)
        {
            try
            {
                _favouriteBookRepository.UpdateFavouriteBook(TypeConverter.ToDAL(book));
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }

        public bool DeleteFavouriteBook(int id)
        {
            try
            {
                _favouriteBookRepository.DeleteFavouriteBook(id);
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }
        #endregion

        #region Comments

        public int CountCommentsByBook(int bookId)
        {
            try
            {
                var count = _commentRepository.CountCommentsByBook(bookId);
                return count;
            }
            catch (System.Data.DataException)
            {
                return 0;
            }
        }

        public IComment CreateComment(IComment comment)
        {
            try
            {
                var id = _commentRepository.CreateComment(TypeConverter.ToDAL(comment));
                comment.Id = id;
                return comment;
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IEnumerable<IComment> GetCommentsByBook(int bookId, int page, int count)
        {
            try
            {
                var items = _commentRepository.ReadComments(bookId, page * count, count);
                return items.Select(x => TypeConverter.FromDAL(x));
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public IComment GetComment(int id)
        {
            try
            {
                var item = _commentRepository.ReadComment(id);
                return item != null ? TypeConverter.FromDAL(item) : null;
            }
            catch (System.Data.DataException)
            {
                return null;
            }
        }

        public bool UpdateComment(IComment comment)
        {
            try
            {
                _commentRepository.UpdateComment(TypeConverter.ToDAL(comment));
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }

        public bool DeleteComment(int id)
        {
            try
            {
                _commentRepository.DeleteComment(id);
                return true;
            }
            catch (System.Data.DataException)
            {
                return false;
            }
        }

        #endregion
    }
}
