﻿using D = DAL.Contracts.DTOs;
using B = BLL.Contracts.DTOs;
using BLL.Domains.DTOs;

namespace BLL.Domains
{
    static class TypeConverter
    {
        public static B.IBook FromDAL(D.IBook item)
        {
            return new Book
            {
                Id = item.Id,
                CategoryId = item.CategoryId,
                Description = item.Description,
                Link = item.Link,
                Title = item.Title,
                Logo = item.Logo,
                Author = item.Author,
                CreationDate = item.CreationDate,
            };
        }

        public static B.ICategory FromDAL(D.ICategory item)
        {
            return new Category
            {
                Id = item.Id,
                Description = item.Description,
                Title = item.Title,
            };
        }

        public static B.IFavouriteBook FromDAL(D.IFavouriteBook item)
        {
            return new FavouriteBook
            {
                Id = item.Id,
                UserId = item.UserId,
                BookId = item.BookId
            };
        }

        public static D.IBook ToDAL(B.IBook item)
        {
            return new Book
            {
                Id = item.Id,
                CategoryId = item.CategoryId,
                Description = item.Description,
                Link = item.Link,
                Title = item.Title,
                Logo = item.Logo,
                Author = item.Author,
                CreationDate = item.CreationDate,
            };
        }

        public static D.ICategory ToDAL(B.ICategory item)
        {
            return new Category
            {
                Id = item.Id,
                Description = item.Description,
                Title = item.Title
            };
        }

        public static D.IFavouriteBook ToDAL(B.IFavouriteBook item)
        {
            return new FavouriteBook
            {
                Id = item.Id,
                UserId = item.UserId,
                BookId = item.BookId
            };
        }

        public static D.IComment ToDAL(B.IComment item)
        {
            return new Comment
            {
                Id = item.Id,
                BookId = item.BookId,
                Text = item.Text,
                UserId = item.UserId,
                CreationDate = item.CreationDate,
            };
        }

        public static B.IComment FromDAL(D.IComment item)
        {
            return new Comment
            {
                Id = item.Id,
                BookId = item.BookId,
                Text = item.Text,
                UserId = item.UserId,
                CreationDate = item.CreationDate,
            };
        }
    }
}
